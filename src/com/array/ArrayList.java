package com.array;

import java.util.List;

public class ArrayList {

	public static void main(String[] args) {
		List<Integer> li = new java.util.ArrayList<Integer>();
		List<Integer> li1 = new java.util.ArrayList<Integer>();
		li.add(30);
		li.add(30);
		li.add(null);
		li.add(10);
		li.add(20);
		li.add(30);
		li.add(40);
		li1.addAll(li);
		li.add(400);
		li.add(200);
		li.add(300);
		li1.add(3000);
		li1.add(50);
		li1.add(1000);
		li1.add(2000);
		
		System.out.println(li);
		System.out.println(li1);
		li1.removeAll(li);
		System.out.println(li1);
		System.out.println("Array list allow the dublicate and null values");
	}


}
