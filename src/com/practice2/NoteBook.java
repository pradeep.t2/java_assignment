package com.practice2;

public class NoteBook extends Book{

	public void draw() {
		System.out.println("draw any diagram in notebook");
	}

	@Override
	public void write() {
		System.out.println("write the answer for given questions");

	}

	@Override
	public void read() {
		System.out.println("read any one paragraph");
		
	}
	
	public static void main(String[] args) {
		NoteBook no= new NoteBook();
		no.draw();
		no.write();
		no.read();
	}
}
