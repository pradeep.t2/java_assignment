package com.practice2;

import java.util.Set;

public class TreeSet {

	public static void main(String[] args) {
		Set<Integer> set = new java.util.TreeSet<>();
		set.add(5);
		set.add(15);
		set.add(25);
		set.add(35);
		set.add(45);
		set.add(55);
		System.out.println(set);
		boolean remove = set.remove(10);
		System.out.println(set);
		set.add(10);
		boolean contains = set.contains(10);
		System.out.println(contains);
		System.out.println(set);
		int size = set.size();
		System.out.println(size);
		System.out.println("Treeset not allow the dublicate values");
	}

}
