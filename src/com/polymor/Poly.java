package com.polymor;

public class Poly {
    // method overloading
	
	private void employeeid (int age) {
		System.out.println(age);
	}
	
	private void employeeid (int id,String name) {
		System.out.println(id + "/n" + name);
	}
	
	private void employeeid (long phno,float regno) {
		System.out.println(phno + "/n" + regno);

		}
		
		
	public static void main(String[] args) {
			Poly p = new Poly();
			p.employeeid(6);
			p.employeeid(12, "vijay");
			p.employeeid(907555566, 658769870);
		}
}
