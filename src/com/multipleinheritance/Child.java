package com.multipleinheritance;

public class Child extends Parent {
	void property3() {
		System.out.println("Childproperty3");
	}

	public static void main(String[] args) {
		
		GrandParent g = new GrandParent();
		g.property1();
		
		Parent p = new Parent();
		p.property1();
		p.propert2();
		
		Child c = new Child();
		c.property1();
		c.propert2();
		c.property3();

	}
}
