package com.practice;

public class Santro extends Car implements BasicCar {

	public void remotestart() {
		System.out.println(" drive mode to automatic mode");
	
	}

	@Override
	public void gearchange() {
		System.out.println(" change 1st gear to 2nd gear");
	}

	@Override
	public void music() {
		System.out.println(" switch on music in car");

	}

	public static void main(String[] args) {
		Santro f=new Santro();
		f.drive();
		f.stop();
		f.remotestart();
		f.gearchange();
		f.music();
		f.lift();
		f.disconnected();
	}
}
