package com.practice;

public class Practice {

	int a = 0;//instance variables
	static int b = 5;//static variable
	public void practice() {
		int c = 20;//local variable
		a=a+5;
		System.out.print(a);
		
	}
	public static void main(String[] args) {
	Practice p = new Practice();
		a=a+20;
		System.out.print(a);//we cannot call a instance variable without creating an object
		b=b+5;
		System.out.print(b);//we cannot call variable withjout creating an object.
        c=c+7;
		System.out.print(c);//the scope of local variable is only in the method.

	}
	
}
