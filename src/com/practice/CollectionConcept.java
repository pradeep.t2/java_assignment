package com.practice;

import java.util.ArrayList;

public class CollectionConcept {

	public static void main(String[] args) {
		
		ArrayList<String> list = new ArrayList<>();
		System.out.println(list.size());
        list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");
		list.add("6");
		System.out.println(list.size());
		
	}
}
