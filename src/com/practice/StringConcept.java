package com.practice;

public class StringConcept {

	public static void main(String[] args) {
		String s1 = "Marriage";
		char[] c = {'s','e','l','e','n','i','u','m'};
		String s2 = "Marrige";
		String s3 = new String("Marriage");
		String s4 = new String("Hello India");
		String s5 = new String(c);
		System.out.println(c);
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		System.out.println(s4);
		s1.concat("java");
		System.out.println(s1);
		s1=s1.concat("java");
	}
}
